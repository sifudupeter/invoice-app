export const state = () => ({
  all: [],
})

export const actions = {
  async getUsers({ commit }) {
    await this.$axios.$get('/user').then((response) => {
      commit('add', response)
    })
  },
}

export const mutations = {
  add(state, users) {
    state.all = users
  },
}
